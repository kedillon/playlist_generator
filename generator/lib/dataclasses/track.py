from dataclasses import dataclass, field


@dataclass
class Track:
    """
    Represents a track.
    """
    track_name: str = field(default="", metadata={"mode": "NULLABLE"})
    track_id: str = field(default="", metadata={"mode": "REQUIRED"})
    album_id: str = field(default="", metadata={"mode": "NULLABLE"})
    artist_name: str = field(default="", metadata={"mode": "NULLABLE"})
    artist_id: str = field(default="", metadata={"mode": "NULLABLE"})
    release_year: str = field(default="", metadata={"mode": "NULLABLE"})
    popularity: int = field(default=0, metadata={"mode": "REQUIRED"})
