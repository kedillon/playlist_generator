from random import randint
from generator.lib.mixins.spotify import Spotify


class Generator(Spotify):

    def __init__(self):
        # Maps a track_id to track metadata
        self.track_data = {}

        # Maps track_id to number of occurrences
        self.my_tracks = {}
        # Maps artist_id to number of occurrences
        self.my_artists = {}

    def ingest_my_music(self):
        """
        Connects to Spotify API and ingests my music
        """
        # Initialize all_my_music with tracks from playlists I own
        all_my_music = self.get_playlist_tracks()

        # Get saved tracks
        limit = 50
        offset = 0
        tracks = self.get_saved_tracks(limit=limit, offset=offset)

        while tracks:
            all_my_music.extend(tracks)
            # Add to offset to get new tracks
            offset += limit

            if len(tracks) == limit:
                tracks = self.get_saved_tracks(limit=limit, offset=offset)
            else:
                tracks = []

        # Reformat track data to my_tracks, track_data, and my_artists
        for track in all_my_music:
            # Add track data to track_data
            if track.track_id not in self.track_data and track.track_id:
                self.track_data[track.track_id] = track

            # Increment number of occurrences of track in my_tracks
            if track.track_id:
                self.my_tracks[track.track_id] = self.my_tracks.get(track.track_id, 0) + 1

            # Increment number of occurrences of artist in my_artists
            if track.artist_id:
                self.my_artists[track.artist_id] = self.my_artists.get(track.artist_id, 0) + 1

        # Add followed artists
        followed = self.get_followed_artists()
        for artist_id in followed:
            self.my_artists[artist_id] = self.my_artists.get(artist_id, 0) + 1

    def get_top_tracks_not_in_my_music(self, artist_id):
        """
        Gets top tracks from an artist that aren't in my music
        :param artist_id: identifies the artist
        :return: list of track ids
        """
        new_tracks = []

        top_tracks = self.get_artist_top_tracks(artist_id)

        for track in top_tracks:
            if track.track_id not in self.my_tracks:
                new_tracks.append(track.track_id)

            if track.track_id not in self.track_data:
                self.track_data[track.track_id] = track

        return new_tracks

    def get_recommended_tracks(self):
        """
        Finds recommended tracks based on my music
        :return: list of track_ids
        """
        # Recommended track ids
        recommended = []

        my_top_artists = sorted(self.my_artists.items(),
                                key=lambda tup: tup[1],
                                reverse=True)
        if len(my_top_artists) > 50:
            my_top_artists = my_top_artists[0:50]

        # Get new tracks for my artists
        for artist, count in my_top_artists:
            recommended.extend(self.get_top_tracks_not_in_my_music(artist))

        # Get recommended artists and their tracks
        related_artists = sorted(
            (self.get_recommended_artists([artist[0] for artist in my_top_artists])).items(),
            key=lambda tup: tup[1],
            reverse=True
        )
        if len(related_artists) > 50:
            related_artists = related_artists[0:50]

        for artist, count in related_artists:
            recommended.extend(
                [track.track_id for track in self.get_artist_top_tracks(artist)]
            )

        return recommended

    def generate_playlist(self, recommended_tracks):
        """
        Generates a playlist with 20 songs
        :param recommended_tracks: list of track ids
        :return: list of Track dataclass objects
        """
        playlist_size = 20

        if len(recommended_tracks) < playlist_size:
            return recommended_tracks

        playlist = []

        while len(playlist) < playlist_size:
            # Chooses a random track index from
            track_idx = randint(0, len(recommended_tracks) - 1)

            # Replace this track if it is a duplicate
            while recommended_tracks[track_idx] in playlist:
                track_idx = randint(0, len(recommended_tracks) - 1)

            playlist.append(recommended_tracks[track_idx])

        playlist_details = [
            self.get_track_details(track) for track in playlist
        ]

        return playlist_details

    def get_track_details(self, track_id):
        """
        Returns track details given a track_id
        :param track_id: identifies the track
        :return: Track dataclass object
        """
        if track_id in self.track_data:
            return self.track_data[track_id]

        return self.track_details(track_id)
