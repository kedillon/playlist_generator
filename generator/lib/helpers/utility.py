from generator.lib.dataclasses.track import Track


def create_track_record(track_details):
    """
    Takes in API response for a track and saves useful information to dataclass
    :param track_details: API response for track
    :return: Track dataclass object
    """
    return Track(
        track_name=track_details.get("name", None),
        track_id=track_details.get("id", None),
        album_id=track_details.get("album", {}).get("id", None),
        artist_name=track_details.get("artists", {})[0].get("name", None),
        artist_id=track_details.get("artists", {})[0].get("id", None),
        release_year=transform_date(track_details.get("album", {})
                                    .get("release_date", None)),
        popularity=track_details.get("popularity", None)
    )


def transform_date(date_string):
    """
    Returns only the year from a date_string
    :param date_string: string in the format year-month-day
    :return: modified date_string - year only
    """
    if not date_string:
        return None
    # otherwise return just the year
    return date_string[0:4]
