import spotipy
import functools

from spotipy import util
from generator.config import CONFIG
from generator.lib.helpers.utility import create_track_record


class Spotify:
    @property
    def config(self):
        return CONFIG.spotify

    @property
    @functools.lru_cache()
    def client(self):
        """
        Reference to a Spotipy client
        """
        token = util.prompt_for_user_token(username=self.config.username,
                                           scope=", ".join(self.config.scope),
                                           client_id=self.config.client_id,
                                           client_secret=self.config.client_secret,
                                           redirect_uri=self.config.redirect_uri)
        return spotipy.Spotify(auth=token)

    # ------------ My Music ------------ #

    @functools.lru_cache()
    def get_saved_tracks(self, limit=50, offset=0):
        """
        Gets my saved tracks
        :param limit: number of tracks to return from spotify
        :param offset: offset for spotify tracks query
        :return: list of track dataclass objects
        """
        result = self.client.current_user_saved_tracks(limit=limit, offset=offset)

        tracks = [
            create_track_record(track["track"])
            for track in result["items"]
        ]
        return tracks

    @functools.lru_cache()
    def get_playlist_tracks(self):
        """
        Gets tracks from current user's playlists
        :return: list of track dataclass objects
        """
        playlists = self.client.current_user_playlists()
        playlist_ids = [playlist["id"] for playlist in playlists["items"]]

        tracks = []
        for playlist in playlist_ids:
            playlist_tracks = self.client.user_playlist_tracks(self.config.username,
                                                               playlist_id=playlist)
            tracks.extend(
                [
                    create_track_record(track["track"])
                    for track in playlist_tracks["items"]
                ]
            )

        return tracks

    @functools.lru_cache()
    def get_followed_artists(self):
        """
        Gets artists current user follows
        :return: list of artist ids
        """
        artists = self.client.current_user_followed_artists()
        return [artist["id"] for artist in artists["artists"]["items"]]

    # ------------ Recommended Music ------------ #

    def get_recommended_artists(self, artist_ids):
        """
        Gets related artists based on a list of artists
        :param: artist_ids: list of artist_ids
        :return: dictionary: maps an artist_id to the weight of that artist
        """
        artists = {}

        for artist_id in artist_ids:
            recommended = self.client.artist_related_artists(artist_id)
            for artist in recommended["artists"]:
                artists[artist["id"]] = artists.get(artist["id"], 0) + 1

        return artists

    @functools.lru_cache()
    def get_artist_top_tracks(self, artist_id):
        """
        Gets the top tracks for a given artist
        :param artist_id: represents the artist
        :return: list of track dataclass objects
        """
        top_tracks = self.client.artist_top_tracks(artist_id=artist_id, country="US")
        return [create_track_record(track) for track in top_tracks["tracks"]]

    @functools.lru_cache()
    def track_details(self, track_id):
        """
        Gets track details from spotify API
        :param track_id: identifies the track
        :return: Track dataclass object
        """

        details = self.client.track(track_id)
        return create_track_record(details)
