import os
import yaml
import logging.config

from box import Box


def setup_logger():
    # get the environment to use for logging
    env = os.environ.get("ENVIRONMENT", "local")
    # set the logging config for entire application using correct yaml file based on environment
    return logging.config.dictConfig(read_config(f"configurations/{env}.logging.yaml"))


def setup_config():
    """
    Returns a dot notation of configuration items.
    :return: Box: dictionary containing configuration items for application, can be accessed via dot notation.
    """
    return Box(
        {
            "spotify": {
                "username": os.environ.get("USERNAME", read_config("configurations/env_vars.yaml")["username"]),
                "client_id": os.environ.get("CLIENT_ID", read_config("configurations/env_vars.yaml")["client_id"]),
                "client_secret": os.environ.get("CLIENT_SECRET", read_config("configurations/env_vars.yaml")["client_secret"]),
                "redirect_uri": os.environ.get("REDIRECT_URI", read_config("configurations/env_vars.yaml")["redirect_uri"]),
                "scope": [
                    "user-read-recently-played",
                    "user-follow-read",
                    "user-library-read",
                    "playlist-read-collaborative",
                    "user-top-read"
                ]
            }
        }
    )


def read_config(filename):
    """
    Helper method for converting yaml config files to dictionary. Filename must be a '.yaml' file.
    :param filename: str: Filename to read/convert.  Must be a '.yaml' file
    :return: dict: dictionary containing configuration options from <filename>
    """
    # for cleanliness get our directory and mark as an internal var _
    _dir = os.path.dirname(os.path.realpath(__file__))
    # get the full path to the config file
    filepath = os.path.join(_dir, f"{filename}")
    # read the yaml file into a dictionary and return the dict
    with open(filepath, "rb") as config:
        return yaml.safe_load(config)


# PATHING
CONFIG_DIR = os.path.dirname(os.path.realpath(__file__))
# Data config
CONFIG = setup_config()
# Logging config
setup_logger()
