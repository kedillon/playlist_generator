from unittest import mock
from generator.lib.helpers.generator import Generator


def test_init():
    gen = Generator()
    assert gen.my_artists == {}
    assert gen.my_tracks == {}
    assert gen.track_data == {}


@mock.patch("generator.lib.mixins.spotify.Spotify.get_followed_artists")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_saved_tracks")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_playlist_tracks")
def test_ingest(mocked_get_playlist_tracks,
                mocked_saved_tracks,
                mocked_followed,
                tracks_in_playlists,
                tracks_saved,
                my_tracks_ingest_result,
                my_artists_ingest_result,
                track_data_ingest_result):
    mocked_get_playlist_tracks.return_value = tracks_in_playlists
    mocked_saved_tracks.side_effect = [tracks_saved, []]
    mocked_followed.return_value = ["artist_id_followed"]

    gen = Generator()
    gen.ingest_my_music()

    assert gen.my_tracks == my_tracks_ingest_result
    assert gen.my_artists == my_artists_ingest_result
    assert gen.track_data == track_data_ingest_result

    assert mocked_saved_tracks.call_count == 1
    mocked_get_playlist_tracks.assert_called_once()
    mocked_followed.assert_called_once()


@mock.patch("generator.lib.mixins.spotify.Spotify.get_artist_top_tracks")
def test_top_tracks_not_in_my_music_empty(mock_get_top_tracks,
                                          artist_top_tracks):
    mock_get_top_tracks.return_value = artist_top_tracks

    gen = Generator()
    new_tracks = gen.get_top_tracks_not_in_my_music("artist_id")

    # empty gen, so all are new
    assert new_tracks == [track.track_id for track in artist_top_tracks]
    assert gen.track_data == {
        track.track_id: track
        for track in artist_top_tracks
    }

    args, _ = mock_get_top_tracks.call_args
    assert args[0] == "artist_id"


@mock.patch("generator.lib.mixins.spotify.Spotify.get_followed_artists")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_saved_tracks")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_playlist_tracks")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_artist_top_tracks")
def test_top_tracks_not_in_my_music_non_empty(mock_get_top_tracks,
                                              mocked_get_playlist_tracks,
                                              mocked_saved_tracks,
                                              mocked_followed,
                                              tracks_in_playlists,
                                              tracks_saved,
                                              track_data_ingest_result,
                                              artist_top_tracks):
    mock_get_top_tracks.return_value = artist_top_tracks
    mocked_get_playlist_tracks.return_value = tracks_in_playlists
    mocked_saved_tracks.side_effect = [tracks_saved, []]
    mocked_followed.return_value = ["artist_id_followed"]

    gen = Generator()
    gen.ingest_my_music()

    new_tracks = gen.get_top_tracks_not_in_my_music("artist_id")

    # empty gen, so all are new
    assert new_tracks == []
    assert gen.track_data == track_data_ingest_result
    args, _ = mock_get_top_tracks.call_args
    assert args[0] == "artist_id"


@mock.patch("generator.lib.mixins.spotify.Spotify.get_followed_artists")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_saved_tracks")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_playlist_tracks")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_recommended_artists")
@mock.patch("generator.lib.mixins.spotify.Spotify.get_artist_top_tracks")
@mock.patch("generator.lib.helpers.generator.Generator.get_top_tracks_not_in_my_music")
def test_get_recommended_tracks(mocked_top_not_in_my_music,
                                mocked_get_artist_top_tracks,
                                mocked_get_recommended,
                                mocked_get_playlist_tracks,
                                mocked_saved_tracks,
                                mocked_followed,
                                tracks_in_playlists,
                                tracks_saved,
                                artist_top_tracks,
                                recommended_result):
    # Ingestion return values
    mocked_get_playlist_tracks.return_value = tracks_in_playlists
    mocked_saved_tracks.side_effect = [tracks_saved, []]
    mocked_followed.return_value = ["artist_id_followed"]

    # Recommend return values
    mocked_top_not_in_my_music.return_value = ["track_id"]
    mocked_get_recommended.return_value = {
        "artist1": 3,
        "artist2": 2,
    }
    mocked_get_artist_top_tracks.return_value = artist_top_tracks

    gen = Generator()
    gen.ingest_my_music()

    recommended = gen.get_recommended_tracks()
    assert recommended == recommended_result

    assert mocked_top_not_in_my_music.call_count == 4
    top_args_list = mocked_top_not_in_my_music.call_args_list
    assert top_args_list[0][0][0] == "artist_id_1"
    assert top_args_list[1][0][0] == "artist_id_2"
    assert top_args_list[2][0][0] == "artist_id_3"
    assert top_args_list[3][0][0] == "artist_id_followed"

    mocked_get_recommended.assert_called_once()
    recommended_args, _ = mocked_get_recommended.call_args
    assert recommended_args[0] == ["artist_id_1",
                                   "artist_id_2",
                                   "artist_id_3",
                                   "artist_id_followed"]

    assert mocked_get_artist_top_tracks.call_count == 2
    top_artist_tracks_arg_list = mocked_get_artist_top_tracks.call_args_list
    assert top_artist_tracks_arg_list[0][0][0] == "artist1"
    assert top_artist_tracks_arg_list[1][0][0] == "artist2"


@mock.patch("generator.lib.helpers.generator.randint")
def test_generate_playlist_short_list(mock_randint):
    recommended_tracks = ["track_id_1, track_id_2"]

    gen = Generator()
    playlist = gen.generate_playlist(recommended_tracks)
    assert playlist == recommended_tracks

    assert mock_randint.call_count == 0


@mock.patch("generator.lib.helpers.generator.Generator.get_track_details")
@mock.patch("generator.lib.helpers.generator.randint")
def test_generate_playlist_long_list(mock_randint, mock_get_track_details):
    recommended_tracks = [f"track_id_{i}" for i in range(50)]
    mock_randint.side_effect = [i for i in range(20)]
    mock_get_track_details.side_effect = [f"details:track_id_{i}" for i in range(20)]

    gen = Generator()
    playlist = gen.generate_playlist(recommended_tracks)
    assert playlist == [f"details:{track}" for track in recommended_tracks[0:20]]

    assert mock_randint.call_count == 20
    assert mock_get_track_details.call_count == 20


@mock.patch("generator.lib.helpers.generator.Generator.get_track_details")
@mock.patch("generator.lib.helpers.generator.randint")
def test_generate_playlist_long_list(mock_randint, mock_get_track_details):
    recommended_tracks = [f"track_id_{i}" for i in range(50)]
    mock_randint.side_effect = ([0, 0, 0, 0, 0] + [i for i in range(20)])
    mock_get_track_details.side_effect = [f"details:track_id_{i}" for i in range(20)]

    gen = Generator()
    playlist = gen.generate_playlist(recommended_tracks)
    assert playlist == [f"details:{track}" for track in recommended_tracks[0:20]]

    assert mock_randint.call_count == 25
    assert mock_get_track_details.call_count == 20


@mock.patch("generator.lib.mixins.spotify.Spotify.track_details")
def test_get_track_details(mock_track_details):
    mock_track_details.return_value = "track_data2"

    gen = Generator()
    gen.track_data = {
        "track_id": "track_data"
    }
    assert gen.get_track_details("track_id") == "track_data"
    assert gen.get_track_details("other_track_id") == "track_data2"
