from box import Box
from unittest import mock
from generator.config import CONFIG
from generator.lib.mixins.spotify import Spotify
from generator.lib.dataclasses.track import Track


def test_config():
    sp = Spotify()
    config  = sp.config

    assert config == Box(
        {
            "username": CONFIG.spotify.username,
            "client_id": CONFIG.spotify.client_id,
            "client_secret": CONFIG.spotify.client_secret,
            "redirect_uri": CONFIG.spotify.redirect_uri,
            "scope": CONFIG.spotify.scope
        }
    )


@mock.patch("generator.lib.mixins.spotify.spotipy.Spotify")
@mock.patch("generator.lib.mixins.spotify.util.prompt_for_user_token")
def test_client(mock_token, mock_client):
    mock_token.return_value = "token"
    mock_client.return_value = "client"

    sp = Spotify()
    client = sp.client

    mock_token.assert_called_once()
    mock_client.assert_called_once()

    assert client == "client"

    _, kwargs = mock_client.call_args
    assert kwargs["auth"] == "token"


@mock.patch("generator.lib.mixins.spotify.Spotify.client")
def test_get_saved(mock_client, saved_api_result):
    mock_client.current_user_saved_tracks.return_value = saved_api_result

    sp = Spotify()
    saved = sp.get_saved_tracks()

    correct = [
        Track(
            track_id="track_id",
            track_name="track_name",
            artist_id="artist_id",
            artist_name="artist_name",
            release_year="2019",
            album_id="album_id",
            popularity=10
        )
    ]

    assert correct == saved


@mock.patch("generator.lib.mixins.spotify.Spotify.client")
def test_playlist_tracks(mock_client, playlist_api_result, saved_api_result):
    mock_client.current_user_playlists.return_value = playlist_api_result
    mock_client.user_playlist_tracks.return_value = saved_api_result

    sp = Spotify()
    playlist_tracks = sp.get_playlist_tracks()

    correct = [
        Track(
            track_id="track_id",
            track_name="track_name",
            artist_id="artist_id",
            artist_name="artist_name",
            release_year="2019",
            album_id="album_id",
            popularity=10
        ),
        Track(
            track_id="track_id",
            track_name="track_name",
            artist_id="artist_id",
            artist_name="artist_name",
            release_year="2019",
            album_id="album_id",
            popularity=10
        )
    ]

    mock_client.current_user_playlists.assert_called_once()
    assert mock_client.user_playlist_tracks.call_count == 2

    args_list = mock_client.user_playlist_tracks.call_args_list
    assert args_list[0][0][0] == CONFIG.spotify.username
    assert args_list[1][0][0] == CONFIG.spotify.username
    assert args_list[0][1]["playlist_id"] == "playlist_id_1"
    assert args_list[1][1]["playlist_id"] == "playlist_id_2"

    mock_client.current_user_playlists.assert_called_once()
    assert mock_client.user_playlist_tracks.call_count == 2

    assert correct == playlist_tracks


@mock.patch("generator.lib.mixins.spotify.Spotify.client")
def test_get_followed_artists(mock_client, artists_api_result):
    mock_client.current_user_followed_artists.return_value = artists_api_result

    sp = Spotify()
    followed_artists = sp.get_followed_artists()

    correct = ["artist_id_1", "artist_id_2"]

    mock_client.current_user_followed_artists.assert_called_once()
    assert correct == followed_artists


@mock.patch("generator.lib.mixins.spotify.Spotify.client")
def test_recommended_artists(mock_client, recommended_artists_api_result):
    mock_client.artist_related_artists.return_value = recommended_artists_api_result

    sp = Spotify()
    recommended_artists = sp.get_recommended_artists(["artist1", "artist2"])

    correct = {
        "artist_id_1": 2,
        "artist_id_2": 2
    }

    args = mock_client.artist_related_artists.call_args_list
    assert args[0][0][0] == "artist1"
    assert args[1][0][0] == "artist2"

    assert mock_client.artist_related_artists.call_count == 2
    assert correct == recommended_artists


@mock.patch("generator.lib.mixins.spotify.Spotify.client")
def test_artist_top_tracks(mock_client, artist_top_tracks_api_result):
    mock_client.artist_top_tracks.return_value = artist_top_tracks_api_result

    sp = Spotify()
    top_tracks = sp.get_artist_top_tracks("artist1")

    correct = [
        Track(
            track_id="track_id",
            track_name="track_name",
            artist_id="artist_id",
            artist_name="artist_name",
            release_year="2019",
            album_id="album_id",
            popularity=10
        )
    ]

    mock_client.artist_top_tracks.assert_called_once()
    assert correct == top_tracks


@mock.patch("generator.lib.mixins.spotify.Spotify.client")
def test_track_details(mock_client, track_api_result):
    mock_client.track.return_value = track_api_result

    sp = Spotify()
    top_tracks = sp.track_details("track_id")

    correct = Track(
        track_id="track_id",
        track_name="track_name",
        artist_id="artist_id",
        artist_name="artist_name",
        release_year="2019",
        album_id="album_id",
        popularity=10
    )

    mock_client.track.assert_called_once()
    assert correct == top_tracks
