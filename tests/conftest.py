import pytest

from generator.lib.dataclasses.track import Track


@pytest.fixture
def tracks_in_playlists():
    return [
        Track(
            track_id="track_id_1",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=0
        ),  # A track
        Track(
            track_id="track_id_2",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=5
        ),  # Different track, same artist and album
        Track(
            track_id="track_id_3",
            album_id="album_id_2",
            artist_id="artist_id_1",
            release_year="release_year_2",
            popularity=2
        ),  # Different track, same artist, different album
        Track(
            track_id="track_id_4",
            album_id="album_id_3",
            artist_id="artist_id_2",
            release_year="release_year_1",
            popularity=1
        ),  # Different track, different artist and album
        Track(
            track_id="track_id_1",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=0
        )   # Duplicate track
    ]


@pytest.fixture
def tracks_saved():
    return [
        Track(
            track_id="track_id_1",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=0
        ),
        Track(
            track_id="track_id_2",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=5
        ),
        Track(
            track_id="track_id_5",
            album_id="album_id_4",
            artist_id="artist_id_3",
            release_year="release_year_1",
            popularity=1
        )
    ]


@pytest.fixture
def saved_api_result():
    return {
        "items": [
            {
                "track": {
                    "album": {
                        "name": "album_name",
                        "id": "album_id",
                        "release_date": "2019-01-05"
                    },
                    "artists": [
                        {
                            "id": "artist_id",
                            "name": "artist_name"
                        }
                    ],
                    "available_markets": [

                    ],
                    "disc_number": 0,
                    "duration_ms": 100000,
                    "explicit": False,
                    "external_ids": {

                    },
                    "external_urls": {

                    },
                    "href": "url",
                    "id": "track_id",
                    "is_local": False,
                    "name": "track_name",
                    "popularity": 10,
                    "preview_url": "url",
                    "track_number": 0,
                    "type": "track",
                    "uri": "uri"
                }
            }
        ]
    }


@pytest.fixture
def playlist_api_result():
    return {
        "items": [
            {
                "id": "playlist_id_1"
            },
            {
                "id": "playlist_id_2"
            }
        ]
    }


@pytest.fixture
def artists_api_result():
    return {
        "artists": {
            "items": [
                {
                    "id": "artist_id_1",
                    "name": "artist_name_1"
                },
                {
                    "id": "artist_id_2",
                    "name": "artist_name_2"
                }
            ],
            "limit": 10
        }
    }


@pytest.fixture
def recommended_artists_api_result():
    return {
        "artists": [
            {
                "id": "artist_id_1",
                "name": "artist_name_1"
            },
            {
                "id": "artist_id_2",
                "name": "artist_name_2"
            }
        ]
    }


@pytest.fixture
def artist_top_tracks_api_result():
    return {
        "tracks": [
            {
                "album": {
                    "name": "album_name",
                    "id": "album_id",
                    "release_date": "2019-01-05"
                },
                "artists": [
                    {
                        "id": "artist_id",
                        "name": "artist_name"
                    }
                ],
                "available_markets": [

                ],
                "disc_number": 0,
                "duration_ms": 100000,
                "explicit": False,
                "external_ids": {

                },
                "external_urls": {

                },
                "href": "url",
                "id": "track_id",
                "is_local": False,
                "name": "track_name",
                "popularity": 10,
                "preview_url": "url",
                "track_number": 0,
                "type": "track",
                "uri": "uri"
            }
        ]
    }


@pytest.fixture
def track_api_result():
    return {
        "album": {
            "name": "album_name",
            "id": "album_id",
            "release_date": "2019-01-05"
        },
        "artists": [
            {
                "id": "artist_id",
                "name": "artist_name"
            }
        ],
        "available_markets": [

        ],
        "disc_number": 0,
        "duration_ms": 100000,
        "explicit": False,
        "external_ids": {

        },
        "external_urls": {

        },
        "href": "url",
        "id": "track_id",
        "is_local": False,
        "name": "track_name",
        "popularity": 10,
        "preview_url": "url",
        "track_number": 0,
        "type": "track",
        "uri": "uri"
    }


@pytest.fixture
def my_tracks_ingest_result():
    return {
        "track_id_1": 3,
        "track_id_2": 2,
        "track_id_3": 1,
        "track_id_4": 1,
        "track_id_5": 1
    }


@pytest.fixture
def my_artists_ingest_result():
    return {
        "artist_id_1": 6,
        "artist_id_2": 1,
        "artist_id_3": 1,
        "artist_id_followed": 1
    }


@pytest.fixture
def track_data_ingest_result():
    return {
        "track_id_1": Track(
            track_id="track_id_1",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=0
        ),
        "track_id_2": Track(
            track_id="track_id_2",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=5
        ),
        "track_id_3": Track(
            track_id="track_id_3",
            album_id="album_id_2",
            artist_id="artist_id_1",
            release_year="release_year_2",
            popularity=2
        ),
        "track_id_4": Track(
            track_id="track_id_4",
            album_id="album_id_3",
            artist_id="artist_id_2",
            release_year="release_year_1",
            popularity=1
        ),
        "track_id_5": Track(
            track_id="track_id_5",
            album_id="album_id_4",
            artist_id="artist_id_3",
            release_year="release_year_1",
            popularity=1
        )
    }


@pytest.fixture
def artist_top_tracks():
    return [
        Track(
            track_id="track_id_1",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=0
        ),  # A track
        Track(
            track_id="track_id_2",
            album_id="album_id_1",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=5
        ),  # Different track, same artist and album
        Track(
            track_id="track_id_3",
            album_id="album_id_2",
            artist_id="artist_id_1",
            release_year="release_year_2",
            popularity=2
        ),  # Different track, same artist, different album
        Track(
            track_id="track_id_4",
            album_id="album_id_3",
            artist_id="artist_id_1",
            release_year="release_year_1",
            popularity=1
        ),  # Different track, different artist and album
    ]


@pytest.fixture
def recommended_result():
    return [
        "track_id",
        "track_id",
        "track_id",
        "track_id",  # From top_not_in_my_music
        "track_id_1",
        "track_id_2",
        "track_id_3",
        "track_id_4",  # From recommended_artist_1
        "track_id_1",
        "track_id_2",
        "track_id_3",
        "track_id_4",  # From recommended_artist_2
    ]
