from generator.lib.helpers.utility import transform_date


def test_transform_date():
    date_in = "2019-04-06"
    assert transform_date(date_in) == "2019"

    date_in = "2012-1-2"
    assert transform_date(date_in) == "2012"

    date_in = None
    assert transform_date(date_in) is None
