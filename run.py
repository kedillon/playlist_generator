from generator.lib.helpers.generator import Generator


def main():
    gen = Generator()
    gen.ingest_my_music()
    recommended = gen.get_recommended_tracks()
    playlist = gen.generate_playlist(recommended)

    for i in range(len(playlist)):
        print(f'{i + 1}.  "{playlist[i].track_name}" by {playlist[i].artist_name}')


if __name__ == "__main__":
    main()