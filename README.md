# Playlist Generator
Playlist Generator ingests a Spotify user's music creates a playlist of recommended songs for that user and writes recommended songs to stdout.

## TOC
1. Requirements
2. Running
3. Testing

## Requirements
Cache exporter requires `Python >= 3.7.3`.

To install dependencies, run the following:\
```pip install -e ."[pbr, test, tox]"```

## Running
### Environment Variables
Playlist Generator uses the following environment variables for configuration.

* `USERNAME`: Identifies the Spotify user
* `CLIENT_ID`: Used for configuring the Spotify API connection
* `CLIENT_SECRET`: Used for configuring the Spotify API connection
* `REDIRECT_URI`: Used for Spotify authorization

Alternatively, variables for configuration can be stored in a file `generator/config/configurations/env_vars.yaml` 

### Running Locally
Cache exporter runs by executing the file `run.py`.
```sh
$ python run.py
```
### Running With Docker
#### Build a Docker Image
To use cache exporter as a docker container you need to first build the Docker image before it can be used.

To build:
```sh
$ docker build -t playlist_generator .
```

#### Using the Docker Image
To run the cache exporter docker image:
```sh
$ docker run playlist_generator
```

Use the -e flag to add any necessary environment variables.
```sh
$ docker run -e USERNAME={username} -e CLIENT_ID={client_id} playlist_generator
```

## Testing
```sh
$ tox
```